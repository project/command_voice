<?php

namespace Drupal\command_voice\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CommandVoiceSettings.
 *
 * @package Drupal\command_voice\Form
 */
class CommandVoiceSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'command_voice.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'command_voice_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('command_voice.settings');
    $data_api = json_decode($config->get('api_info'));
    $total = $config->get('api_info_data');
    $form['#tree'] = TRUE;

    $num_names = $form_state->get('num_names');
    $form['#tree'] = TRUE;
    if (empty($num_names)) {
      $fieldset_count = $total;
      if ($fieldset_count) {
        $num_names = $form_state->set('num_names', $fieldset_count);
      }
      else {
        $num_names = $form_state->set('num_names', $total);
      }
    }

    $form['names_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Command Voice'),
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $form_state->get('num_names'); $i++) {
      $form['names_fieldset']['inner'][$i] = [
        '#type' => 'fieldset',
        '#title' => $this->t($i+1 . ' Command Voice Setting '),
        '#prefix' => '<div id="names-fieldset-wrapper">',
        '#suffix' => '</div>',
      ];
      $form['names_fieldset']['inner'][$i]['speak_voice_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Speak Voice Text'),
        '#default_value' => $data_api->data[$i]->speak_voice_text,
      ];
      $form['names_fieldset']['inner'][$i]['speak_open_url'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Speak Open Url'),
        '#default_value' => $data_api->data[$i]->speak_open_url,
      ];
    }

    $form['names_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['names_fieldset']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'names-fieldset-wrapper',
      ],
    ];
    if ($form_state->get('num_names') > 1) {
      $form['names_fieldset']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'names-fieldset-wrapper',
        ],
        '#limit_validation_errors' => [],
      ];
    }

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();

    $api_info = ['data' => $values['names_fieldset']['inner']];
    $data = $this->config('command_voice.settings');

    $data->set('api_info', json_encode($api_info));
    $data->set('api_info_data', count($api_info['data']));

    $data->save();

   \Drupal::messenger()->addMessage('Command Voice Settings Saved');
  }

  /**
   * Add more ajax callback.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['names_fieldset'];
  }

  /**
   * AddOne Callback.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_names');
    $add_button = $name_field + 1;
    $form_state->set('num_names', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Remove Callback.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_names');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_names', $remove_button);
    }
    $form_state->setRebuild();
  }

}
