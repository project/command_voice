(function ($, Drupal) {
  Drupal.behaviors.command_voice = {
    attach: function (context, settings) {

      window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;

      const recognition = new SpeechRecognition();
      recognition.interimResults = true;

      recognition.addEventListener('result', e => {
        transcript = Array.from(e.results).map(result => result[0]).map(result => result.transcript).join('');
        const statement = e.results[0][0].transcript;
        console.log(statement);


      var enable = JSON.parse(drupalSettings.command_voice.data);
      const myarray = Object.values(enable);
      //console.log(enable);
      //const array1 = ['a', 'b', 'c'];
      const iterator = myarray.values();

      for (const value of iterator) {
        for (const val of value){

          if (statement === val.speak_voice_text) {
          console.log(val.speak_voice_text + ' -' + val.speak_open_url);
            window.location.href = val.speak_open_url;
          }
        }
      }

      });

      // When we stop talking, start the process again, so it'll record when we start
      // talking again.
      recognition.addEventListener('end', recognition.start);

      recognition.start();







    }
  }
})(jQuery, Drupal);
